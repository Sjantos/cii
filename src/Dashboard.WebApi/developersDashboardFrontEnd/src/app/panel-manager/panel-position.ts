export interface PanelPosition {
    column: number;
    row: number;
}